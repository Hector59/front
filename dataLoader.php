<?php
function callAPI($method, $url, $data){
   $curl = curl_init();
   switch ($method){
      case "POST":
         curl_setopt($curl, CURLOPT_POST, 1);
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         break;
      case "PATCH":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
         break;
      default:
         if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
   }
   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);
	if(isset($_POST["id"])){
	   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	      'Content-Type: application/merge-patch+json',
	      'accept: application/json',
	   ));
	}
	else{
	   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	      'Content-Type: application/json',
	   ));
	}
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
   // EXECUTE:
   $result = curl_exec($curl);
   //var_dump($result);
   if(!$result){die("Connection Failure");}
   curl_close($curl);
   return $result;
}

$nom=$_POST["nom"];
$type=$_POST["type"];
$taille=$_POST["taille"];

if(isset($_POST["id"])){
	$id=$_POST["id"];
	$url="http://localhost:8000/api/produits/$id";
	var_dump($url);
	$data_array =  array("ïd"=>$id,"nom"=>$nom,"type"=>$type,"taille"=>$taille);
	$make_call = callAPI("PATCH", "http://localhost:8000/api/produits/$id", json_encode($data_array));
	//var_dump($make_call);
}
else{
	$data_array =  array("nom"=>$nom,"type"=>$type,"taille"=>$taille);
	$make_call = callAPI("POST", "http://localhost:8000/api/produits", json_encode($data_array));
}

header('Location: index.php');
?>