<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="style.css" />
    <title>Liste des produits</title></head>
<body>
  <table border=1 class="greenTable">
    <thead>
      <tr>
        <th colspan=5>Liste des produits</th>
      </tr>
      <tr>
        <th>Nom</th>
        <th>Type</th>
        <th>Taille</th>
        <th>Supprimer</th>
        <th>Modifier</th>
      </tr>
    </thead>

    <tfoot>
      <tr>
        <td colspan=5>
          <form action="add.html">
            <input type="submit" value="Ajouter un produit" style="margin-top: 15px;" />
          </form>
        </td>
      </tr>
    </tfoot>

    <tbody id="tableProduits">
      
    </tbody>
  </table>
</body>

<?php
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "http://localhost:8000/api/produits",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "cache-control: no-cache"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  $response = json_decode($response, true);
  //var_dump($response);
?>


<script type="text/javascript">
  var tableProduits=document.getElementById('tableProduits');
  var response=<?= json_encode($response);?>;

  var a = 0;
  for (var item in response) {
    a++;
    if(a==4){
      for(var i = 0;i<response[item].length;i++){

        console.log('---------------------');
        console.log(response[item][i]["id"]);
        console.log(response[item][i]["Nom"]);
        console.log(response[item][i]["Type"]);
        console.log(response[item][i]["Taille"]);
        console.log('---------------------');

        var newLine=document.createElement('tr');

        var newCell=document.createElement('td');
        newCell.innerHTML=response[item][i]["Nom"];
        newLine.appendChild(newCell);

        var newCell=document.createElement('td');
        newCell.innerHTML=response[item][i]["Type"];
        newLine.appendChild(newCell);

        var newCell=document.createElement('td');
        newCell.innerHTML=response[item][i]["Taille"];
        newLine.appendChild(newCell);


        var newCell=document.createElement('td');

        var f = document.createElement("form");
        f.setAttribute('method',"post");
        f.setAttribute('action',"delete.php");

        var id = response[item][i]["id"];
        var nom = response[item][i]["nom"];
        var type = response[item][i]["type"];
        var taille = response[item][i]["taille"];

        var j = document.createElement("input");
        j.setAttribute('type',"hidden");
        j.setAttribute('name',"id");
        j.setAttribute('value',id);
        f.appendChild(j);

        var s = document.createElement("input");
        s.setAttribute('type',"submit");
        s.style.width="100%";
        s.setAttribute('value',"❌");
        f.appendChild(s);


        newCell.appendChild(f);

        newLine.appendChild(newCell);

        var newCell=document.createElement('td');

        var f2 = document.createElement("form");
        f2.setAttribute('method',"post");
        f2.setAttribute('action',"update.php");

        var id = response[item][i]["id"];

        var k = document.createElement("input");
        k.setAttribute('type',"hidden");
        k.setAttribute('name',"id");
        k.setAttribute('value',id);
        f2.appendChild(k);

        var inputNom = document.createElement("input");
        inputNom.setAttribute('type',"hidden");
        inputNom.setAttribute('name',"nom");
        inputNom.setAttribute('value',nom);
        f2.appendChild(inputNom);


        var inputType = document.createElement("input");
        inputType.setAttribute('type',"hidden");
        inputType.setAttribute('name',"type");
        inputType.setAttribute('value',type);
        f2.appendChild(inputType);


        var inputTaille = document.createElement("input");
        inputTaille.setAttribute('type',"hidden");
        inputTaille.setAttribute('name',"taille");
        inputTaille.setAttribute('value',taille);
        f2.appendChild(inputTaille);

        var s = document.createElement("input");
        s.setAttribute('type',"submit");
        s.style.width="100%";
        s.setAttribute('value',"📝");

        f2.appendChild(s);

        newCell.appendChild(f2);

        newLine.appendChild(newCell);


        tableProduits.appendChild(newLine);
      }
    }
  }
</script>
</html>