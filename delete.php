<?php
$curl = curl_init();
$id = $_POST['id'];
curl_setopt_array($curl, array(
  CURLOPT_URL => "http://localhost:8000/api/produits/$id",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "DELETE",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

//var_dump($err);
//var_dump($id);

curl_close($curl);
$response = json_decode($response, true);
header('Location: index.php');
?>