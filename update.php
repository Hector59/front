<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="style.css" />
		<title>Mettre à jour un produit:</title>
	</head>

	<body>
		<form action="dataLoader.php" method="POST">
			<input name="id" id="id" type="hidden" value= <?= $_POST['id'] ?> >
			<table border=1 class="greenTable">
				<thead>
					<tr>
						<th colspan=2>Mettre à jour un produit:<br>
					</tr>
				</thead>

				<tfoot>
					<tr>
						<td colspan=5>
							<input type="submit" value="Enregistrer" style="margin-top: 15px;" />
						</td>
					</tr>
				</tfoot>

				<tbody>
					<tr>
						<td>Nom:</td>
						<td><input name="nom" id="nom" value= <?= $_POST['nom'] ?>></td>
					</tr>
					<tr>
						<td>Type:</td>
						<td><input name="type" id="type" value= <?= $_POST['type'] ?>></td>
					</tr>
					<tr>
						<td>Taille:</td>
						<td><input name="taille" id="taille" value= <?= $_POST['taille'] ?>></td>
					</tr>
				</tbody>
			</table>
		</form>
	</body>
</html>